#!/bin/zsh
DIR=$(dirname $0)
[[ ${DIR} =~ '^.$' ]] && DIR=$(pwd)
ln -sf "$DIR/attack_alerts" /etc/logcheck/cracking.ignore.d/codefix_local
ln -sf "$DIR/security_events" /etc/logcheck/violations.ignore.d/codefix_local
ln -sf "$DIR/system_events" /etc/logcheck/ignore.d.server/codefix_local
ln -sf "$DIR/violations" /etc/logcheck/violations.d/codefix_local
